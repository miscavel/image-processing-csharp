# Image Processing C#

C# form with various functions to alter .png and .jpg RGB images.

Note :
1. Project is built in Visual Studio 2010.
2. For academic purposes, most of the algorithms are self-coded albeit not optimized.

Example :

Original Image

![](Screenshots/Midgardsormr.png)

Functions :
1. [Gray Convert](https://www.tutorialspoint.com/dip/gray_level_resolution.htm) (Level 2)

![](Screenshots/Midgardsormr_modified_gray-level-2.png)

2. [Neighbor Zoom x Bilinear Zoom](https://www.mathworks.com/help/vision/ug/interpolation-methods.html) (Enlarge * 2.0)

![](Screenshots/Midgardsormr_modified_neighbor-zoom-x2.png)
![](Screenshots/Midgardsormr_modified_bilinear-zoom-x2.png)

3. [Negative](https://www.tutorialspoint.com/dip/gray_level_transformations.htm)

![](Screenshots/Midgardsormr_modified_negative.png)

4. [Log Transform x Power Law](https://www.tutorialspoint.com/dip/gray_level_transformations.htm) (c = 1, gamma = 5)

![](Screenshots/Midgardsormr_modified_log-1-5.png)
![](Screenshots/Midgardsormr_modified_power-1-5.png)

5. [Histogram Equalization](https://www.tutorialspoint.com/dip/histogram_equalization.htm)

![](Screenshots/Midgardsormr_modified_histogram.png)

6. [Contrast Stretching](https://academic.mu.edu/phys/matthysd/web226/Lab01.htm)

![](Screenshots/contrast_stretching.png)

![](Screenshots/Midgardsormr_modified_contrast-stretching.png)

7. [Laplacian Filter](https://homepages.inf.ed.ac.uk/rbf/HIPR2/log.htm)

![](Screenshots/Midgardsormr_modified_laplace.png)

8. [Sobel Filter](https://medium.com/datadriveninvestor/understanding-edge-detection-sobel-operator-2aada303b900)

![](Screenshots/Midgardsormr_modified_sobel.png)

9. Modified Sobel

![](Screenshots/Midgardsormr_modified_fossil.png)

10. [Color Complement](https://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/OWENS/LECT14/lecture12.html) 

![](Screenshots/Midgardsormr_modified_color-complement.png)

11. [Fourier Transform : High Pass Filter - Gaussian](http://biomedpharmajournal.org/vol7no2/image-sharpening-by-gaussian-and-butterworth-high-pass-filter/) (n = 3, d0 = 10)

![](Screenshots/Midgardsormr_modified_hp-gaussian-3-10.png)
![](Screenshots/hp_gauss_3_10.png)

12. [Fourier Transform : High Pass Filter - Ideal](https://www.tutorialspoint.com/dip/High_Pass_vs_Low_Pass_Filters.htm)

(n = 3, d0 = 10)

![](Screenshots/Midgardsormr_modified_hp-ideal-3-10.png)
![](Screenshots/hp_ideal_3_10.png)

(n = 3, d0 = 50)

![](Screenshots/Midgardsormr_modified_hp-ideal-3-50.png)
![](Screenshots/hp_ideal_3_50.png)

13. [Fourier Transform : Low Pass Filter - Butterworth](http://mstrzel.eletel.p.lodz.pl/mstrzel/pattern_rec/filtering.pdf) (n = 3, d0 = 50)

![](Screenshots/Midgardsormr_modified_lp-butter-3-50.png)
![](Screenshots/lp_butter_3_50.png)

14. [Fourier Transform : Low Pass Filter - Ideal](https://www.tutorialspoint.com/dip/High_Pass_vs_Low_Pass_Filters.htm) (n = 3, d0 = 50)

![](Screenshots/Midgardsormr_modified_lp-ideal-3-50.png)
![](Screenshots/lp_ideal_3_50.png)

15. [Wavelet Function](http://gwyddion.net/documentation/user-guide-en/wavelet-transform.html)

![](Screenshots/wavelet.png)

Salt & Peppered Image

![](Screenshots/Midgardsormr_modified_salt-pepper.png)

16. [Midpoint Filter](http://www.roborealm.com/help/Mid.php)

![](Screenshots/Midgardsormr_modified_salt-pepper_modified_midpoint.png)

17. [Median Filter](https://courses.cs.washington.edu/courses/cse576/07sp/notes/Basics2_white.pdf)

![](Screenshots/Midgardsormr_modified_salt-pepper_modified_median.png)

18. [Mean Filter](https://courses.cs.washington.edu/courses/cse576/07sp/notes/Basics2_white.pdf)

![](Screenshots/Midgardsormr_modified_salt-pepper_modified_mean.png)

19. [Harmonic Mean Filter x Geometric Mean Filter](https://www.toppr.com/guides/business-mathematics-and-statistics/measures-of-central-tendency-and-dispersion/harmonic-geometric-mean/)

![](Screenshots/Midgardsormr_modified_salt-pepper_modified_harmonic-mean.png)
![](Screenshots/Midgardsormr_modified_salt-pepper_modified_geometric-mean.png)

20. [Contra-Harmonic Mean Filter (Q = 1) x (Q = -1)](https://www.blackice.com/Help/Tools/Document%20Imaging%20SDK%20webhelp/WebHelp/Contra-Harmonic_Mean_Filter.htm)

![](Screenshots/Midgardsormr_modified_salt-pepper_modified_contraharmonic-mean-p.png)
![](Screenshots/Midgardsormr_modified_salt-pepper_modified_contraharmonic-mean-n.png)

